<?php
    //Database variables
    $dbHost = "localhost";
    $dbName = "cs75finance";
    $dbUser = "rmpressler";
    $dbPass = "harvard.cs75";
    $connectString = "mysql:host=$dbHost;port=3306;dbname=$dbName";
    
    //Location variables
    $sName = "localhost";
    $homeLocation = "Location: http://$sName/index.php?page=home";
    $portfolioLocation = "Location: http://$sName/index.php?page=portfolio";
    
    define("PORTFOLIO_LOCATION", "Location: http://$sName/index.php?page=portfolio");
    
    //Functionality variables
    $startMoney = 10000.00;
    
    /*******************************************************
    * string getQuote(string $symbol)
    *
    * Used to get a dollar value of a US stock.
    *
    * string $symbol = 4-character ticker symbol, case 
    *                  insensitive
    *
    * [RETURNS] string containing decimal dollar value
    *           of the given stock
    *******************************************************/
    
    function getQuote($symbol) {
        //Error checking ###############################################################
        if ($symbol == "") {                    //Check for blank entry         
            $e = "noSymbol";
        } else if (!ctype_alnum($symbol) 
                   || strlen($symbol) > 4) {    //Ensure provided symbol meets  
            $e = "badFormat";                   //standard for US common stock symbol
        }                                       //(alpha-numeric and 4 chars or less)
        
        //Send error, if found
        if (isset($e)) {
            error(PORTFOLIO_LOCATION, "eQuote", $e);
        }
        //End error checking ############################################################
        
        $symbol = urlencode($symbol);
        
        //Open CSV and store contents in array $csv
        $url = "http://download.finance.yahoo.com/d/quotes.csv?s=$symbol&f=sl1d1t1c1ohgv&e=.csv";
        $handle = fopen($url, "r");
        $csv = fgetcsv($handle);
        fclose($handle);
        
        //Detect an empty CSV result
        if ($csv[1] == "N/A") {
            error(PORTFOLIO_LOCATION, "eQuote", "notExist");
        }
        
        return $csv[1];
    }
    
    /***********************************************************
    * string dollarize(mixed $input)
    *
    * Accepts a string or int/float/double and inserts
    * commas and restricts numbers after the decimal
    * to two places.
    *
    * mixed $input = string containing only numbers, or a number
    *                stored as such.
    *
    * [RETURNS] string containing input number with
    *           commas and only showing 2 decimal
    *           places.
    *
    * EXAMPLE:
    * $result = dollarize(1000000);
    *
    * $result would contain "1,000,000.00"
    ************************************************************/
    
    function dollarize($input) {
        return number_format($input, 2, ".", ",");
    }
    
    /***********************************************************
    * array querySingle(PDO $dbh, string $statement)
    *
    * Runs a $statement as a PDO prepared statement. This method
    * is safer than executing non-prepared, non-PDO statements
    * as PDO prepared statements are parsed for unsafe code prior
    * to running. Returns the resulting single row table.
    *
    * PDO $dbh = PDO handle for given database connection
    * string $statement = string containing SQL query
    *
    * [RETURNS] an array containing a single row, each element
    *           corresponding to a column.
    ***********************************************************/
    
    function querySingle($dbh, $statement) {
        $query = $dbh->prepare($statement);
        $query->execute();
        $queryArray = $query->fetch();
        return $queryArray;
    }
    
    /**********************************************************
    * void error(string $URL, string $type, string $argStr)
    * 
    * Used to redirect user to sending page and hand the page
    * the corresponding error to handle.
    *
    * string $URL = Contains absolute URL to receiving page
    * string $type = Type of error, i.e. 'eSell', 'eQuote'
    *                that will alert the page as to where
    *                to display the error message.
    * string $argStr = Code used by the receiving page to
    *                  determin what to say in the error msg.
    *********************************************************/ 
    
    function error($URL, $type, $argStr) {
        header($URL . "&$type=" . $argStr);
        die;
    }
    
    /********************************************************
    * string getCashBalance(PDO $dbh, string $userID)
    *
    * Gets the remaining cash balance of a user.
    *
    * PDO $dbh = Contains PDO object created when DB
    *            connection was established.
    * string $userID = Numeric userID for the currently
    *                  logged in user.
    *
    * [RETURNS] string containing numeric cash balance of 
    *           user.
    *********************************************************/
    
    function getCashBalance($dbh, $userID) {
        $select = "SELECT Balance
                   FROM Users
                   WHERE UserID='$userID';";
        $balanceArray = querySingle($dbh, $select);
        return $balanceArray[0];
    }
    
    /********************************************************
    * void setCashBalance(PDO $dbh, mixed $newBalance,
    *                     mixed $userID)
    *
    * Replaces user's current Balance with $newBalance
    *
    * PDO $dbh = Contains PDO object created when DB
    *            connection was established.
    * mixed $newBalance = Balance to be inserted. May be 
    *                     numeric string or a number stored
    *                     as such.
    * mixed $userID = UserID of current user. Max be numeric
    *                 string or a number stored as such.
    *********************************************************/
    
    function setCashBalance($dbh, $newBalance, $userID) {
        $update = "UPDATE Users
                   SET Balance=$newBalance
                   WHERE UserID=$userID;";
        querySingle($dbh, $update);
    }
    
    /*********************************************************
    * void setShares(PDO $dbh, mixed $newShares,
    *                mixed $newBasis, mixed $holdingID)
    *
    * Replaces holding's current shares with $newShares
    *
    * PDO $dbh = Contains PDO object created when DB
    *            connection was established.
    * mixed $newShares = Shares to be inserted. May be numeric
    *                    string or literal number.
    * mixed $newBasis = New cost basis to be inserted. May be
    *                   numeric string or a literal number.
    * mixed $holdingID = HoldingID of the holding in Holdings
    *                    table. May be numeric string or literal
    *                    number.
    ************************************************************/
    
    function setShares($dbh, $newShares, $newBasis, $holdingID) {
        $update = "UPDATE Holdings 
                   SET Shares=$newShares, Basis=$newBasis 
                   WHERE HoldingID=$holdingID;";
        querySingle($dbh, $update);
    }
    
    /************************************************************
    * void renderError(string $element, string $class, string $code)
    *
    * Used by views to determine which error message to display
    * based on passed in error arg.
    *
    * string $element = contains either 'span' or 'div', which
    *                   will be used to determine which type
    *                   of HTML element to show.
    * string $class = contains CSS classes.
    * string $code = contains code passed in by error()
    *************************************************************/
    
    function renderError($element, $class, $code) {
        echo "<$element class='$class'>";
          switch ($code) {
              case "noSymbol":
                  echo "Please enter a symbol to quote!";
                  break;
              case "badFormat":
                  echo "That is not a US common stock ticker symbol";
                  break;
              case "notExist":
                  echo "That is not a US common stock ticker symbol";
                  break;
              case "both":
                  echo "Please enter EITHER shares or dollars.";
                  break;
              case "empty":
                  echo "Please enter shares or dollars to buy.";
                  break;
              case "shareNAN":
                  echo "Please enter whole number of shares.";
                  break;
              case "dollarNAN":
                  echo "Please enter numeric dollar amount.";
                  break;
              case "insufficientCash":
                  echo "Insufficient funds!";
                  break;
              case "nonNum":
                  echo "Must enter a numeric value to sell!";
                  break;
              case "notInt":
                  echo "Must enter a whole number to sell shares!";
                  break;
              case "insufficientShares":
                  echo "You don't have that many shares!";
                  break;
              case "incompleteReg":
                  echo "Please complete all fields.";
                  break;
              case "nameAlpha":
                  echo "Names may only contain letters!";
                  break;
              case "nameLength":
                  echo "Names cannot be longer than 20 letters!";
                  break;
              case "userChar":
                  echo "Username can only contain letters and numbers.";
                  break;
              case "userLength":
                  echo "Username cannot be more than 16 characters.";
                  break;
              case "passChar":
                  echo "Password can only contain letters and numbers.";
                  break;
              case "passLength":
                  echo "Password must be between 6 and 20 characters.";
                  break;
              case "passMatch":
                  echo "Passwords do not match!</span>";
                  break;
              case "userTaken":
                  echo "Username is in use!</span>";
                  break;
              case "negative":
                  echo "Please enter a positive number";
                  break;
              case "incompleteLogin":
                  echo "You must enter username AND password";
                  break;
              case "invalidLogin":
                  echo "Username or password invalid.";
                  break;
              case "incorrectLogin":
                  echo "Username or password incorrect.";
                  break;
              case "nologin":
                  echo "You are not logged in!";
                  break;
          }
          echo "</$element>";
    }
?>
