<?php
/********************************************************
* void renderConfPanel(string $conf, mixed $shares,
*                      string $symbol, mixed $price)
* 
* Mostly HTML, used to print a confirmation panel if
* the $_GET['conf'] variable is set.
*
* string $conf = contains 'bought' or 'sold'
* mixed $shares = contains the number of shares bought or
*                 sold to be printed. May be numeric
*                 string or literal number.
* string $symbol = contains 4-character ticker symbol for
*                  stock traded.
* mixed $price = contains per-share price at which the 
*                trade executed.
* ********************************************************/                      

function renderConfPanel($conf, $shares, $symbol, $price) {
    $totalTransaction = dollarize($shares * $price);
    echo "<div class='panel panel-success'>
            <div class='panel-heading'>
              <h3 class='panel-title'>Trade Confirmation</h3>
            </div>
            <div class='panel-body money'>
              You've successfully $conf $shares shares of $symbol for \$$price each.<br>
              Total transaction: \$$totalTransaction
            </div>
          </div>";
}

/**********************************************************
* void renderRow(string $symbol, mixed $shares, mixed $price,
*                mixed $balance, $mixed basis, string $gainLoss
*                string $sell)
*
* (A '%' character indicates an optional parameter.)
* 
* Prints a single table row with given input.
*
* string $symbol = string containing 4-character ticker
*                  symbol to be printed in first column.
* %mixed $shares = Number of shares currently owned. May
*                 be numeric string or literal number.
* %mixed $price = Current price of stock per-share. May be
*                numeric string or literal number.
* mixed $balance = Current market value of total holding. May
                   be numeric string or literal number.
* mixed $basis = Current cost basis. May be numeric string or
*                literal number.
* %string $gainLoss = String containing both dollar and 
*                     percent representation of gain/loss.
* %string $sell = Contains string of HTML to show the sell
*                 form.
***********************************************************/
                 

function renderRow($symbol, $shares = "", $price = "", $balance, $basis, $gainLoss = "", $sell = "") {
    if ($price != 0) {
        $price = "\$" . $price;
    }
    echo "<tr>
              <td>$symbol</td>
              <td>$shares</td>
              <td>$price</td>
              <td>\$$balance</td>
              <td>\$$basis</td>
              <td>$gainLoss</span></td>
              <td>
                $sell
              </td>
            </tr>";
}

/***********************************************************
* void getRows(PDO $connection)
*
* Retrieves all records in Holdings that have OwnerID of
* the current user, calculates non-stored information, and
* passes it to renderRow for printing to screen.
*
* PDO $connection = contains current PDO connection to DB.
***********************************************************/

function getRows($connection) {
    //Connect to database
    try {
        $dbh = $connection;
    }
    catch (PDOException $e) {
        echo $e->getMessage();
    }
    
    //Prepare and execute query to get holding info
    $userID = $_SESSION['userID'];
    $select = "SELECT Symbol, Shares, Basis, HoldingID FROM Holdings WHERE OwnerID = $userID";
    $holdingQuery = $dbh->prepare($select);
    $holdingQuery->execute();
    
    //Get user's current balance
    $cash = getCashBalance($dbh, $_SESSION['userID']);
    
    $totals = array("balance" => $cash, "costBasis" => $cash);
    
    //Print holding rows
    foreach($holdingQuery->fetchAll() as $row) {
        extract($row);
        
        //Perform calculations
        $price = getQuote($row[0]);
        $balance = $Shares * $price;
        $dlrChange = $balance - $Basis;
        $pctChange = number_format((($dlrChange / $balance) * 100), 2);
        
        //Set color based on pos/neg dollar change
        if ($dlrChange >= 0) {
            $color = "text-success";
        }
        else {
            $color = "text-danger";
        }
        
        //Increment running totals
        $totals['balance'] += $balance;
        $totals['costBasis'] += $Basis;
        
        //Prepare dollar amounts for printing and fill $sell
        $balance = dollarize($balance);
        $Basis = dollarize($Basis);
        $dlrChange = dollarize($dlrChange);
        $gainLoss = "<span class='$color'>\$$dlrChange ($pctChange%)</span>";
        $sell = "<form action='sell.php' method='post'>
                  <input type='hidden' name='holdingID' value='$HoldingID'>
                  <input type='text' class='input-sm' size='4' name='quantity'><select class='input-sm select-sell' name='units'>
                    <option value='shares'>Shares</option>
                    <option value='dollars'>Dollars</option>
                  </select><button type='submit' class='btn btn-default btn-sm input-sm'>Go</button>
                </form>";
        
        renderRow($Symbol, $Shares, $price, $balance, $Basis, $gainLoss, $sell);
    }
    
    //Append cash row
    $cash = dollarize($cash);
    renderRow("Cash", "", "", $cash, $cash);
    
    //Assemble total row  
    
    //Calculate dollar and percent change and determine proper color to display      
    $dlrChange = $totals['balance'] - $totals['costBasis'];
    if ($dlrChange >= 0) {
        $color = "text-success"; //Bootstrap code that turns text green by default
    }
    else {
        $color = "text-danger";  //Bootstrap code that turns text red by default
    }
    $pctChange = dollarize((($dlrChange / $totals['balance']) * 100));
    
    //Prepare dollar amounts for printing
    $dlrChange = dollarize($dlrChange);
    $totals['balance'] = dollarize($totals['balance']);
    $totals['costBasis'] = dollarize($totals['costBasis']);
    $gainLoss = "<span class='$color'>\$$dlrChange ($pctChange%)</span>";
    
    //Append total row
    renderRow("Total", "", "", $totals['balance'], $totals['costBasis'], $gainLoss);        
}

/*********************************************************
* void renderQuotePanel(string $symbol, mixed $quote,
*                       string $eBuy)
*
* string $symbol = Contains 4-character ticker symbol for
*                  the stock that was requested.
* mixed $quote = Current price of $symbol as returned by
*                quote.php and/or getQuote()
* %string $eBuy = Error, if any, to be displayed in the
*                 quote panel.
*********************************************************/

function renderQuotePanel($symbol, $quote, $eBuy = "") {
    $upperSymbol = strtoupper($symbol);
    echo "<br><div class='panel panel-primary quote'>
            <div class='panel-heading'>
              <h3 class='panel-title'>$upperSymbol</h3>
            </div>
            <div class='panel-body money'>
              $upperSymbol is currently trading at <strong>\$$quote</strong> per share.";
    
    if ($eBuy != "") {
        echo "<br>";
        renderError('span', 'text-danger', $eBuy);
    }          
              
    echo "  <form class='form-inline' action='buy.php' method='post'>
                <div class='pull-left'>
                  <h4>Purchase:</h4>
                  <div class='form-group'>
                    <input class='input-control' type='text' name='buyShares'> shares
                  </div>
                  <br>OR<br>
                  <div class='form-group'>
                    <input class='input-control' type='text' name='buyDollars'> dollars
                  </div>
                </div>
                <div class='pull-right'>
                  <br>Once you click
                  <br>'Buy' your order
                  <br>will be placed!
                  <br><button type='submit' name='symbol' value='$symbol' class='btn btn-default'>Buy</button>
                </div>
              </form>
            </div>
          </div>";
}
?>
