Richard Pressler
richard@richardpressler.com OR rmpressler@gmail.com
Goffstown, NH - based developer
Completed current version 6/7/2015

project1.zip contains a local version of the CS75 Finance website as developed by Richard Pressler

This website was a project assigned to students of Harvard's CS75 course: Creating Dynamic Websites. The assignment was to create a 
website that allows users to create their own accounts and buy and sell stocks with hypothetical money, starting with $10,000.
Project specifications: http://cdn.cs75.net/2012/summer/projects/1/project1.pdf

Technologies required to be used were:
-PHP
--PDO (optionally)
-MySQL
-HTML/CSS
-git version control

I also chose to use the following:
-PHPass library to add password encryption
-Bootstrap CSS to speed up development of front-end

Development environment:
-Ubuntu 14.04
-gedit
-LAMPP package
-Chrome, Firefox, IE10 on Windows 8.1 for testing

Site File Structure:

/html
    index.php - main controller
    buy.php
    sell.php
    register.php
    login.php
    logout.php
    /css
        [various CSS files]
    /fonts
        [icons packaged with Bootstrap]
    /img
        [images used by HTML]
    /js
        [Bootstrap JS files]
/includes
    helpers.php - contains various helper functions and 'global' variables
    portfolio.inc.php - functions used in displaying content on views/portfolio.php
    PasswordHash.php - Courtesy of http://www.openwall.com/phpass/
/views
    home.php
    portfolio.php
    /templates
        header.php
        footer.php
