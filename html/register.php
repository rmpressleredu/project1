<?php
    session_start();
    require("../includes/helpers.php");
    require("../includes/PasswordHash.php");
    extract($_POST);
    
    //Error checking ---------------------------------------------------------
    
    $errorURL = $homeLocation;
    
    
    if ($username == ""                         //Check for field completion
        || $password == ""
        || $firstName == ""
        || $lastName == ""
        || $passwordConf == "") {
            $e = "incompleteReg";
    } else if (!ctype_alpha($firstName)         //Check names for letters only
        || !ctype_alpha($lastName)) {           
        $e = "nameAlpha";
    } else if (strlen($firstName) > 20          //Check names for max length
                || strlen($lastName) > 20) {    
        $e = "nameLength";
    } else if (!ctype_alnum($username)) {       //Check username for alphanumeric only
        $e = "userChar";
    } else if (strlen($username) > 16) {        //Check for username length
        $e = "userLength";
    } else if (!ctype_alnum($password)) {       //Check password for alphanumeric only
        $e = "passChar";
    } else if (strlen($password) > 20           //Check for password length
        || strlen($password) < 6) {             
        $e = "passLength";
    } else if ($password != $passwordConf) {    //Check pass and passConf for matching
        $e = "passMatch";
    }
    
    if (isset($e)) {
        error($errorURL, "eReg", $e);
    }
    
    //End error checking -----------------------------------------------------
    
    //Initialize hash
    $hasher = new PasswordHash(8, false);
    $hash = $hasher->HashPassword($password);
    
    //Connect to database
    try {
        $dbh = new PDO($connectString, $dbUser, $dbPass);
    }
    catch (PDOException $e) {
        echo $e->getMessage();
    }
    
    //Verify Username availability
    $userVerify = "SELECT 1 FROM Users WHERE username='$username'";
    $userQuery = $dbh->prepare($userVerify);
    $userQuery->execute();
    if (!$userQuery->fetchColumn() === false) {
        $userQuery = NULL;
        header($homeLocation . "&eReg=userTaken");
        die();
    }
    
    //Stylize names
    $firstName = ucfirst(strtolower($firstName));
    $lastName = ucfirst(strtolower($lastName));
    
    //Insert new user to database
    $insert = "INSERT INTO Users (Username, Password, FirstName, LastName, Balance) 
               VALUES ('$username', '$hash', '$firstName', '$lastName', $startMoney);";
    querySingle($dbh, $insert);
    
    //Get new user's UserID
    $select = "SELECT UserID FROM Users WHERE Username = '$username';";
    $idArray = querySingle($dbh, $select);
    $userID = $idArray[0];
    
    $_SESSION['userID'] = $userID;
    header($portfolioLocation);
    die();
?>
