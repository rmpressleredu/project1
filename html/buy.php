<?php
    session_start();
    require("../includes/helpers.php");
    
    extract($_POST);
    $quote = getQuote($symbol);
    
    $errorURL = $portfolioLocation . "&symbol=$symbol&quote=$quote";
    $symbol = strtoupper($symbol);
    
    //Error checking #################################################################
    if ($buyShares != "" && $buyDollars != "") {      //Ensure only one field filled out
        $e = "both";
    } else if ($buyShares == "" && $buyDollars == "") {
        $e = "empty";
    } else if ($buyShares != "") {                 //Ensure $buyShares is whole number
        if (!ctype_digit($buyShares)) {
            $e = "shareNAN";
        }
    } else if ($buyDollars != "") {                
        if (!is_numeric($buyDollars)) {             //Ensure $buyDollars is numeric
            $e = "dollarNAN";
        } else if($buyDollars <= 0) {               //Ensure $buyDollars is positive
            $e = "negative";
        }
        else {
            $buyShares = floor($buyDollars / $quote);
        }
    }
    
    //Send error, if found
    if (isset($e)) {
        error($errorURL, "eBuy", $e);
    }
    //End error checking ###############################################################
    
    $buyCost = $buyShares * $quote;
    
    //Connect to database
    try {
        $dbh = new PDO($connectString, $dbUser, $dbPass);
    }
    catch (PDOException $e) {
        echo $e->getMessage();
    }
    
    //Ensure user has enough cash
    $currentBalance = getCashBalance($dbh, $_SESSION['userID']);
    
    if ($currentBalance < $buyCost) {
        error($errorURL, "eBuy", "insufficientCash");
    }
    
    //Check whether user already holds this stock
    $select = "SELECT HoldingID, Shares, Basis
               FROM Holdings
               WHERE OwnerID='{$_SESSION['userID']}' AND Symbol='$symbol';";
    $selectArray = querySingle($dbh, $select);
    
    //If it's a new stock for this user, add the record to Holdings
    if (!isset($selectArray[0])) {
        $selectQuery = NULL;
    
        //Add stock to user's holdings
        $insert = "INSERT INTO Holdings (Symbol, Shares, Basis, OwnerID)
                   VALUES ('$symbol', $buyShares, $buyCost, {$_SESSION['userID']});";
        querySingle($dbh, $insert);
    }
    
    //If user already holds this stock, update existing record
    else {
        extract($selectArray);
        
        //Update existing holding with new shares
        $newShares = $Shares + $buyShares;
        $newBasis = $Basis + $buyCost;
        setShares($dbh, $newShares, $newBasis, $HoldingID);
    }
    
    //Reduce cash balance
    $newBalance = $currentBalance - $buyCost;
    setCashBalance($dbh, $newBalance, $_SESSION['userID']);
    
    header($portfolioLocation . "&conf=bought&shares=$buyShares&symbol=$symbol&price=$quote");
    die();
?>
