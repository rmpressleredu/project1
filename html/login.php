<?php
    session_start();
    require("../includes/helpers.php");
    require("../includes/PasswordHash.php");

    extract($_POST);
    
    //Error checking ####################################################################
    
    //Returns $e="incomplete" if either field left blank
    if ($username == "" || $password == "") {
        $e = "incompleteLogin";
    }
    //Returns $e="invalid" if...
    else if (strlen($username) > 16   //$username too long
        || strlen($password) < 6      //$password too short
        || strlen($password) > 20     //$password too long
        || !ctype_alnum($username)    //$username contains non-alphanumeric characters
        || !ctype_alnum($password)) { //$username contains non-alphanumeric characters
        $e = "invalidLogin";
    }
    
    //Send error, if found
    if(isset($e)) {
        error($homeLocation, "eLogin", $e);
    }
    
    //End Error checking ################################################################
    
    //Connect to database
    try {
        $dbh = new PDO($connectString, $dbUser, $dbPass);
    }
    catch (PDOException $e) {
        echo $e->getMessage();
    }
    
    //Get user's password hash
    $select = "SELECT UserID, Password 
               FROM Users 
               WHERE Username='$username'";
    $loginArray = querySingle($dbh, $select);
    $dbHash = $loginArray['Password'];
    
    //Throw $e="incorrect" if $username is not found in database
    if (!$dbHash) {
        error($homeLocation, "eLogin", "incorrectLogin");
    }
    
    //Check password validity
    $hasher = new PasswordHash(8, false);
    $check = $hasher->CheckPassword($password, $dbHash);
    
    if ($check) {
        $_SESSION['userID'] = $loginArray['UserID'];
        header($portfolioLocation);
        die();
    }
    else {
        error($homeLocation, "eLogin", "incorrectLogin");
    }
?>
