<?php
    session_start();
    require("../includes/helpers.php");

    //Set $page
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    }
    else {
        $page = "home";
    }
    
    extract($_GET);
    
    //Error catching---------------------------------------------------
    
    //If portfolio is requested, but user not logged in, throw error
    if ($page == "portfolio"  && !isset($_SESSION['userID'])) {
        $page = "home";
        $e = "nologin";
    }
    else if ($page == "home" && isset($_SESSION['userID'])) {
        $page = "portfolio";
    }
    
    //End error catching-----------------------------------------------
    
     
    //Render appropriate page
    require("../views/templates/header.php");

    require("../views/$page.php");
    
    require("../views/templates/footer.php");
?>
