<?php
    session_start();
    require("../includes/helpers.php");
    
    extract($_POST);
    
    $errorURL = $portfolioLocation;
    
    //Error checking ############################################################
    if (!is_numeric($quantity)) {       //Error if $quantity not a number
        $e = "nonNum";
    } else if ($units == 'shares'       //Error if $shares not whole number
        && !ctype_digit($quantity)) {    
        $e = "notInt";
    } else if ($quantity <= 0) {        //Error if either is negative
        $e = "negative";
    }
    
    //Send error, if found
    if (isset($e)) {
        error($errorURL, "eSell", $e);
    }
    //End error checking ##########################################################
    
    //Connect to database
    try {
        $dbh = new PDO($connectString, $dbUser, $dbPass);
    }
    catch (PDOException $e) {
        echo $e->getMessage();
    }
    
    //Get $shares and $Symbol from database
    $select = "SELECT Shares, Symbol, Basis 
               FROM Holdings WHERE HoldingID=$holdingID;";
    $result = querySingle($dbh, $select);
    extract($result);
    
    $price = getQuote($Symbol);
    
    //Set number of shares to be sold
    if ($units == "dollars") {
        $sellShares = floor($quantity / $price);
    } 
    else {
        $sellShares = $quantity;
    }
    
    //Ensure user can afford purchase
    if ($quantity > $Shares) {
        error($errorURL, "eSell", "insufficientShares");
    }
    
    //If $newShares is 0, delete that record. Otherwise, update.
    $newShares = $Shares - $sellShares;
    if ($newShares == 0) {
        //Delete record
        $delete = "DELETE FROM Holdings 
                   WHERE HoldingID=$holdingID;";
        querySingle($dbh, $delete);
    }
    else {
        //Calculate new cost basis
        $basisPerShare = $Basis / $Shares;
        $newBasis = $Basis - ($basisPerShare * $sellShares);
    
        //Set new shares
        setShares($dbh, $newShares, $newBasis, $holdingID);
    }
    
    //Get user's current balance
    $balance = getCashBalance($dbh, $_SESSION['userID']);
    
    //Set new balance
    $newBalance = $balance + ($sellShares * $price);
    setCashBalance($dbh, $newBalance, $_SESSION['userID']);
    
    //Close connection
    $dbh = NULL;
    header($portfolioLocation . "&conf=sold&shares=$sellShares&symbol=$Symbol&price=$price");
    die();
?>
