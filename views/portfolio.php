<?php require("../includes/portfolio.inc.php"); ?>
<section class="container portfolio">
  <div class="row visible-xs">
    <div class="col-sm-12" id="mobile-pad">
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-sm-12">
      <h1>Portfolio</h1>
      <?php 
          if(isset($conf)) { 
              renderConfPanel($conf, $shares, $symbol, $price); 
          }
      ?>
    </div>
    <div class="col-md-6 col-sm-12 quote">
      <div class="well">
        <h2>Quote <small>To buy additional stock, first request a quote!</small></h2>
        <?php 
            if (isset($eQuote)) {
                renderError('span', 'text-danger', $eQuote);
            }
        ?>
        <form action="quote.php" class="form-inline" role="form" method="get">
          <div class="form-group">
            <label for="symbol">Enter a symbol to get a quote: </label>
            <input class="form-control" name="symbol" id="symbol" type="text" placeholder="XXXX">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-default">Get Quote</button>
          </div>
        </form>
          <?php
          if (isset($quote)) { 
              if (isset($eBuy)) {
                  renderQuotePanel($symbol, $quote, $eBuy);
              } else { renderQuotePanel($symbol, $quote); }
          }
        ?>
      </div>
    </div>
  </div>
  <?php
      if(isset($eSell)) {
          renderError('div', 'alert-danger pull-right', $eSell);
      }
  ?>
  <div class="row">
    <div class="col-md-12">
      <div class="table-responsive">
        <table class="table table-bordered money">
          <thead>
            <tr>
              <th>Symbol</th>
              <th>Shares</th>
              <th>Market Value (per Share)</th>
              <th>Current Balance</th>
              <th>Cost Basis</th>
              <th>Gain/Loss (%)</th>
              <th>Sell</th>
            </tr>
          </thead>
          <tbody>
            <?php getRows($dbh); ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
