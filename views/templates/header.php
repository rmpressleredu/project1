<?php
    if ($page == "portfolio") {
        //Connect to DB
        try {
            $dbh = new PDO($connectString, $dbUser, $dbPass);
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }
        
        //Get first name and balance for use in navbar
        $select = "SELECT FirstName, Balance 
                           FROM Users WHERE UserID='{$_SESSION['userID']}'";
        $result = querySingle($dbh, $select);
        extract($result);
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <link href='http://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">
  <script src="scripts/bootstrap.min.js"></script>
  <title><?php echo ucfirst($page) . " - "; ?>CS75 Finance</title>
</head>
<body>
<header class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <img class="navbar-brand" src="img/cs75finance.jpg">
    </div>
    <div class="navbar-right">
    <?php 
    //Render the log-in menu if $page is "home"
        if ($page == "home") { ?> 
          <form action="login.php" class="navbar-form form-inline" role="form" method="post">
          <?php 
            if (isset($eLogin)) {
                renderError('span', 'text-danger', $eLogin);
            } 
          ?>
            <div class="form-group">
              <input type="text" name="username" class="form-control" placeholder="Username">
            </div>
            <div class="form-group">
              <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-user"></span> Login</button>
          </form>
    <?php 
    //Render the welcome message, balance, and log-out button if $page is "portfolio"
        } else if ($page == "portfolio") { ?>
          <form action="logout.php" class="navbar-form form-inline" role="form">
            <div class="form-group">
              <label>Welcome, <?php echo $FirstName; ?>!</label>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-default">Log Out</button>
            </div>
          </form>
    <?php
        }
    ?>
    </div>
  </div>
</header>
