<section>
<div class="container">
  <div class="row">
    <div class="col-md-4 register">
      <h2>Create an account now!</h2>
      <p>Think you've got what it takes to buy and sell stocks? Try it out with C$75 Finance, using C-Dollars (C$), play money for you to test your skill!</p>
      <?php
          if (isset($eReg)) {
              renderError("div", "alert alert-danger", $eReg);
          }
      ?>
      <form action="register.php" role="form" method="post">
        <label>Name:</label>
        <div class="form-group row">
          <div class="col-sm-6">
            <input type="text" class="form-control" name="firstName" id="firstName" placeholder="First Name">
          </div>
          <div class="col-sm-6">
            <input type="text" class="form-control" name="lastName" id="lastName" placeholder="Last Name">
          </div>
        </div>
        <div class="form-group">
          <label for="username">Choose a username:</label>
          <input type="text" class="form-control" name="username" id="username" placeholder="Up to 16 alpha-numeric characters">
        </div>
        <div class="form-group">
          <label for="password">Choose a password:</label>
          <input type="password" class="form-control" name="password" id="password" placeholder="6-20 alpha-numeric characters">
        </div>
        <div class="form-group">
          <label for="passwordConf">Enter password again:</label>
          <input type="password" class="form-control" name="passwordConf" id="passwordConf" placeholder="Enter password again to confirm">
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Create your free account!</button>
        </div>
      </form>
    </div>
  </div>
</div>
</section>
